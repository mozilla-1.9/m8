<!-- ***** BEGIN LICENSE BLOCK *****
#if 0
   - Version: MPL 1.1/GPL 2.0/LGPL 2.1
   -
   - The contents of this file are subject to the Mozilla Public License Version
   - 1.1 (the "License"); you may not use this file except in compliance with
   - the License. You may obtain a copy of the License at
   - http://www.mozilla.org/MPL/
   -
   - Software distributed under the License is distributed on an "AS IS" basis,
   - WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
   - for the specific language governing rights and limitations under the
   - License.
   -
   - The Original Code is DOM Inspector.
   -
   - The Initial Developer of the Original Code is
   - Netscape Communications Corporation.
   - Portions created by the Initial Developer are Copyright (C) 2003
   - the Initial Developer. All Rights Reserved.
   -
   - Contributor(s):
   -   Shawn Wilsher <me@shawnwilsher.com>
   -
   - Alternatively, the contents of this file may be used under the terms of
   - either the GNU General Public License Version 2 or later (the "GPL"), or
   - the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
   - in which case the provisions of the GPL or the LGPL are applicable instead
   - of those above. If you wish to allow use of your version of this file only
   - under the terms of either the GPL or the LGPL, and not to allow others to
   - use your version of this file under the terms of the MPL, indicate your
   - decision by deleting the provisions above and replace them with the notice
   - and other provisions required by the LGPL or the GPL. If you do not delete
   - the provisions above, a recipient may use your version of this file under
   - the terms of any one of the MPL, the GPL or the LGPL.
   -
#endif
   - ***** END LICENSE BLOCK ***** -->

  <!ENTITY cmdShowFindDialog.label "Rechercher un nœud…">
  <!ENTITY cmdShowFindDialog.accesskey "R">
  <!ENTITY cmdShowFindDialog.accelkey "r">

  <!ENTITY cmdFindNext.label "Rechercher le suivant">
  <!ENTITY cmdFindNext.accesskey "s">

  <!ENTITY cmdToggleAnonContent.label "Afficher le contenu anonyme">
  <!ENTITY cmdToggleAnonContent.accesskey "A">

  <!ENTITY cmdToggleWhitespaceNodes.label "Afficher les nœuds vides">
  <!ENTITY cmdToggleWhitespaceNodes.accesskey "v">

  <!ENTITY cmdToggleAccessibleNodes.label "Afficher les nœuds accessibles">
  <!ENTITY cmdToggleAccessibleNodes.accesskey "c">

  <!ENTITY cmdToggleProcessingInstructions.label "Afficher les instructions de traitement">
  <!ENTITY cmdToggleProcessingInstructions.accesskey "h">

  <!ENTITY cmdFlashSelected.label "Faire clignoter l'élément sélectionné">

  <!ENTITY cmdToggleAttributes.label "Afficher les attributs">
  <!ENTITY cmdToggleAttributes.accesskey "t">

  <!ENTITY cmdSelectByClick.label "Sélectionner l'élément par un clic">
  <!ENTITY cmdSelectByClick.accesskey "c">

  <!ENTITY ColumnsDialog.title "Colonnes">

  <!ENTITY cmdShowColumnsDialog.label "Colonnes…">
  <!ENTITY cmdShowColumnsDialog.accesskey "o">
  
  <!ENTITY cmdInsertNode.label "Insérer…">
  <!ENTITY cmdDeleteNode.label "Supprimer">
  <!ENTITY cmdCopyXML.label "Copier le code de l'élément">
  <!ENTITY cmdBlink.label "Faire clignoter l'élément">
  <!ENTITY cmdInspectBrowser.label "Examiner le contenu du document">
  <!ENTITY cmdInspectInNewWindow.label "Examiner dans une nouvelle fenêtre">
  
  <!ENTITY cmdShowPseudoClasses.label "Afficher les styles des pseudo-classes…">
  <!ENTITY cmdShowPseudoClasses.accesskey "p">
  
  <!ENTITY pseudoClasses.title "Pseudo-classes">

  <!ENTITY findNodes.title "Rechercher des nœuds">
  <!ENTITY findNodesById.label "Id">
  <!ENTITY findNodesByTag.label "Balise">
  <!ENTITY findNodesByAttr.label "Attribut">
  <!ENTITY findNodesByAttrValue.label "Valeur">
  <!ENTITY findNodesSearchBy.label "Chercher par">
  <!ENTITY findNodesDirection.label "Direction">
  <!ENTITY findNodesDirectionUp.label "En haut">
  <!ENTITY findNodesDirectionDown.label "En bas">
  <!ENTITY findNodesFind.label "Rechercher">
  <!ENTITY findNodesCancel.label "Annuler">

  <!ENTITY mnEditPasteMenu.label "Coller">
  <!ENTITY mnEditPasteBefore.label "Avant">
  <!ENTITY mnEditPasteAfter.label "Après">
  <!ENTITY mnEditPasteReplace.label "À la place de (Remplacer)">
  <!ENTITY mnEditPasteFirstChild.label "Comme premier enfant">
  <!ENTITY mnEditPasteLastChild.label "Comme dernier enfant">
  <!ENTITY mnEditPasteAsParent.label "Comme parent">

  <!ENTITY mnEditInsertMenu.label "Insérer">
  <!ENTITY mnEditInsertAfter.label "Après">
  <!ENTITY mnEditInsertBefore.label "Avant">
  <!ENTITY mnEditInsertFirstChild.label "Comme premier enfant">
  <!ENTITY mnEditInsertLastChild.label "Comme dernier enfant">

  <!ENTITY insertNode.title "Insérer un nœud">
  <!ENTITY nodeType.label "Type de nœud :">
  <!ENTITY nodeType.element.label "Élément">
  <!ENTITY nodeType.text.label "Texte">
  <!ENTITY namespaceURI.label "URI de l'espace de noms :">
  <!ENTITY tagName.label "Nom de la balise :">
  <!ENTITY nodeValue.label "Valeur du nœud :">

  <!ENTITY namespaceTitle.null.label "null">
  <!ENTITY namespaceTitle.default.label "Espace de noms par défaut du document">
  <!ENTITY namespaceTitle.custom.label "Personnalisé">

<!-- LOCALIZATION NOTE: The following entities are names of W3C specifications.
                        Please consult http://www.w3.org/Consortium/Translation/
                        for appropriate translation or leave unchanged. -->
  <!ENTITY namespaceTitle.XMLNS.label "XMLNS">
  <!ENTITY namespaceTitle.XML.label "XML">
  <!ENTITY namespaceTitle.XHTML.label "XHTML">
  <!ENTITY namespaceTitle.XLink.label "XLink">
  <!ENTITY namespaceTitle.XSLT.label "XSLT">
  <!ENTITY namespaceTitle.XBL.label "XBL">
  <!ENTITY namespaceTitle.MathML.label "MathML">
  <!ENTITY namespaceTitle.RDF.label "RDF">
  <!ENTITY namespaceTitle.XUL.label "XUL">
  <!ENTITY namespaceTitle.SVG.label "SVG">
  <!ENTITY namespaceTitle.XMLEvents.label "Événements XML">
  <!ENTITY namespaceTitle.WAIRoles.label "Rôles WAI">
  <!ENTITY namespaceTitle.WAIProperties.label "Propriétés WAI">
