pref("qa.extension.litmus.url", "http://litmus.mozilla.org/");
pref("qa.extension.hermes.url", "http://litmus.mozilla.org/hermes/1/");
pref("qa.extension.isFirstTime", true);
pref("qa.extension.minNotificationInterval", 5400000); // 90 minutes
pref("qa.extension.lastNotificationTime", 0);
pref("qa.extension.minNotificationCheckInterval", 5400000); // 90 minutes
pref("qa.extension.lastNotificationCheckTime", 0);
pref("qa.extension.notificationSettings", '0,1,0,1,0,0,1');